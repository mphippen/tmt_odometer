# -*- coding: utf-8 -*-
"""Project metadata

Information describing the project.
"""

# The package name, which is also the "UNIX name" for the project.
package = 'TMT_Position_Readings'
project = "TMT_Position_Readings WebServices"
project_no_spaces = project.replace(' ', '')
version = '0.1'
description = 'Provides interface with Omnitracs web services'
authors = ['Tyler Marrs', 'Michael Phippen']
authors_string = ', '.join(authors)
emails = ['tmarrs@crst.com', 'mphippen@crst.com']
license = 'MIT'
copyright = '2017 ' + authors_string
url = 'http://crst.com/'
