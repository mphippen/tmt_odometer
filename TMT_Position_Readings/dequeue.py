import argparse
import copy
import json
import logging
import pymssql as ps
import time
from datetime import datetime

import pytz
from omnitracs import ess
from CustomQueries.SQL_SERVER_QUERIES import check_for_unit_exist, insert_into_position_readings

logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser()
parser.add_argument("configFile", help="File path to JSON config file, see template")
args = parser.parse_args()
config = json.loads(open(args.configFile).read())

user = config['OMNITRACS_USER']
password = config['OMNITRACS_PASS']

unique_inserts = {}
missing_arr = []

total_count = 0
success_count = 0

directions = ['North', 'East', 'South', 'West']

with ps.connect(server=config['unit_server'], user=config['SQL_USER'], password=config['SQL_PASS']) as u_conn:
    with u_conn.cursor(as_dict=True) as u_cursor:
        with ps.connect(server=config['reading_server'], user=config['SQL_USER'], password=config['SQL_PASS'],
                        database='OmnitracsInterface') as r_conn:
            with r_conn.cursor(as_dict=True) as r_cursor:
                queue = ess.ESS(user, password, config['sub_id'], config)

                # Create generator so we yield a block of transactions at a time
                block_iter = iter(queue.dequeue(output_file=config['output_file']))

                while block_iter:
                    block = next(block_iter, None)
                    if not block:
                        break  # For each of the transactions in the block returned by omnitracs..
                    for tran_id, tran in block['transactions'].items():
                        total_count += 1

                        # Confirm we have unit in UNITS table
                        u_cursor.execute(check_for_unit_exist.format(tran['equipment_ID'], tran['equipment_ID']))
                        row = u_cursor.fetchone()

                        if row:
                            success_count += 1
                            # Convert to Central time
                            input_zone = pytz.timezone('GMT')
                            output_zone = pytz.timezone('US/Central')
                            event_date = input_zone.localize(
                                datetime.strptime(tran['eventTS'], '%Y-%m-%dT%H:%M:%SZ'))
                            position_date = input_zone.localize(
                                datetime.strptime(tran['position_posTS'], '%Y-%m-%dT%H:%M:%SZ'))

                            tran['eventTS'] = event_date.astimezone(output_zone).strftime('%Y-%m-%d %H:%M:%S')
                            tran['position_posTS'] = position_date.astimezone(output_zone).strftime(
                                '%Y-%m-%d %H:%M:%S')

                            tran['companyID'] = config['company']

                            # Add to our insert list if first reading for unit
                            if tran['equipment_ID'] not in unique_inserts:
                                unique_inserts[tran['equipment_ID']] = copy.deepcopy(tran)
                                unique_inserts[tran['equipment_ID']]['equipment_ID'] = row['UNITID']
                                unique_inserts[tran['equipment_ID']]['tran_ID'] = tran_id

                            # Else replace if this is a newer reading
                            elif datetime.strptime(tran['position_posTS'], "%Y-%m-%d %H:%M:%S") > \
                                    datetime.strptime(unique_inserts[tran['equipment_ID']]['position_posTS'],
                                                      "%Y-%m-%d %H:%M:%S"):
                                unique_inserts[tran['equipment_ID']] = copy.deepcopy(tran)
                                unique_inserts[tran['equipment_ID']]['equipment_ID'] = row['UNITID']
                                unique_inserts[tran['equipment_ID']]['tran_ID'] = tran_id

                        else:
                            if not missing_arr.__contains__(tran['equipment_ID']):
                                missing_arr.append(tran['equipment_ID'])

                # Insert most recent of records
                for uID in unique_inserts:

                    unique_inserts[uID]['headingText'] = 'None'
                    if unique_inserts[uID]['heading']:
                        heading = int(float(unique_inserts[uID]['heading']))
                        if heading % 90 == 0:
                            unique_inserts[uID]['headingText'] = directions[int(heading / 90)]

                    query = insert_into_position_readings.format(
                            unique_inserts[uID]['tran_ID'],
                            unique_inserts[uID]['eventTS'],
                            unique_inserts[uID]['equipment_ID'],
                            unique_inserts[uID]['equipment_equipType'],
                            unique_inserts[uID]['equipment_unitAddress'],
                            unique_inserts[uID]['equipment_mobileType'],
                            unique_inserts[uID]['auxID'],
                            unique_inserts[uID]['position_lat'],
                            unique_inserts[uID]['position_lon'],
                            unique_inserts[uID]['position_posTS'],
                            unique_inserts[uID]['ignitionStatus'],
                            'T.2.06.0',
                            unique_inserts[uID]['proximity_city'],
                            unique_inserts[uID]['proximity_stateProv'],
                            unique_inserts[uID]['proximity_postal'],
                            unique_inserts[uID]['proximity_country'],
                            unique_inserts[uID]['proximity_placeType'],
                            unique_inserts[uID]['proximity_distance'],
                            unique_inserts[uID]['proximity_direction'],
                            unique_inserts[uID]['ltdDistance'],
                            unique_inserts[uID]['speed'],
                            unique_inserts[uID]['heading'],
                            unique_inserts[uID]['headingText'],
                            unique_inserts[uID]['odometer'],
                            unique_inserts[uID]['companyID'],
                            time.strftime('%Y-%m-%d %H:%M:%S'))
                    r_cursor.execute(query)
                    r_conn.commit()

                # Print stats
                logger.warning("Successful Readings: {}/{}\n".format(success_count, total_count))
                logger.warning("Unique Units: {}\n".format(len(unique_inserts)))

                # Print out missing units
                logger.warning("Units with Omnitracs reading but not found in TMT:\n")
                for index, unit in enumerate(missing_arr):
                    logger.warning("{}".format(str(unit)))
