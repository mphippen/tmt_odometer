check_for_unit_exist = """
SELECT
    TOP 1 UNITID
FROM
    [TFW].[dbo].[UNITS]
WHERE
    (
    [UNITNUMBER] = '{}' or
    [ASSETNUM] = '{}'
    ) and
    [TYPE] like '%tractor%' and
    [STATUS] = 'ACTIVE'
"""

insert_into_position_readings = """
    INSERT INTO
        [OmnitracsInterface].[dbo].[TMT_Position_Readings]
        (TxnID, EventTS, Equipment, Type, UnitAddress, MobileType, AuxID, Lat, Lon, PosTS,
            Ign, MsgTxnType, Prox1City, Prox1State, Prox1Zip, Prox1Country, Prox1PlaceType,
            Prox1Distance, Prox1Direction, LTDdistance, Speed, HeadingSent, Heading, Odometer,
            CompanyID, InsertTS)
    VALUES
        ({}, '{}', '{}', '{}', '{}', '{}', {}, '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', {}, '{}', '{}')
"""
